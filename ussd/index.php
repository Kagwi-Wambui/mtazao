<?php
// Reads the variables sent via POST from our gateway
$sessionId   = $_POST["sessionId"];
$serviceCode = $_POST["serviceCode"];
$phoneNumber = $_POST["phoneNumber"];
$text        = $_POST["text"];
$level = 0;

//Appending * to splid responses
$ussd_string_exploded = explode ("*",$ussd_string);

if ($text == "") {
    // Introduction menu
    $response  = "CON Welcome to Mtazao \n";
    $response .= "1. Register as a farmer \n";
    $response .= "2. Register as a buyer \n";
    $response .= "3. About Mtazao \n" ;
    $response .= "4. Contact Us" ;
    //$response .= "5. Confirm transaction" \n;

} else if ($text == "1") {
    // Prompt famrmer to enter details
    $response = "CON Farmer Details: \n";
    $response .= "1. Enter full names \n";
    $response .= "2. Enter phone number \n";
    $response .= "3. Product \n";
    $response .= "4. Quantity of produce(in kg) \n";
    $response .= "5. Season of harvest \n";
    $response .= "6. Location";

}else if($text == "1*1") { 
    $text = "Enter your Full Names:";
    $name  = "";
    $response = "CON Name: ".$name;

} else if($text == "1*2") { 
    $text = "Enter your Phone Number:";
    $phone_number  = "";
    $response = "CON Phone Number: ".$phone_number;

}  else if($text == "1*3") { 
    $text = "Enter Product:";
    $product  = "";
    $response = "CON Product: ".$product;

}  else if($text == "1*4") { 
    $text = "Enter Quantity:";
    $quantity  = "";
    $response = "CON Quantity: ".$quantity;

}  else if($text == "1*5") { 
    $text = "Enter Season:";
    $season  = "";
    $response = "CON Season: ".$season;

}else if($text == "1*6") { 
    $text = "Enter Location:";
    $location  = "";
    $response = "END Location: ".$location;
    

} else if ($text == "2") {
    // Prompt buyer to enter details
    $response = "CON Buyer Details: \n";
    $response .= "1. Enter full names \n";
    $response .= "2. Enter phone number \n";
    $response .= "3. Location";

} else if($text == "2*1") { 
    $text = "Enter your Full Names:";
    $bname  = "";
    $response .= "CON Name: ".$bname;

} else if($text == "2*2") { 
    $text = "Enter your Phone Number:";
    $bphone_number  = "";
    $response .= "CON Phone Number: ".$bphone_number;

}  else if($text == "2*3") { 
    $text = "Enter Location:";
    $blocation  = "";
    $response .= "CON Location: ".$blocation;
    $response .= "END Details successfully added  \n";

} else  if ($text == "3") {
    // Description on Mtazao
    $response = "END Mtazao is a mobile platform that connects farmers and buyers directly.For more information log on to our website. \n";

    //else if to else

} else  if ($text == "4") {
    // Description on Mtazao
    $response = "END In case of any queries and concerns contact us on:0712-345-678 or visit our website for more information. Or send an SMS to 26641";

    //else if to else
}
//else if($text == "4") { 
// Driver's part to confirm delivery
//$response = "Driver details ".$;


// Echo the response back to the API
header('Content-type: text/plain');
echo $response;