<?php
// Reads the variables sent via POST from our gateway
$sessionId   = $_POST["sessionId"];
$serviceCode = $_POST["serviceCode"];
$phoneNumber = $_POST["phoneNumber"];
$text        = $_POST["text"];
//$level = explode(“*”, $text);

//Appending * to split responses
//$ussd_string_exploded = explode ("*",$ussd_string);

if ($text == "") {
    // Introduction menu
    $response  = "CON Welcome to Mtazao \n";
    $response .= "1. Register as a farmer \n";
    $response .= "2. Register as a buyer \n";
    $response .= "3. About Mtazao \n" ;
    $response .= "4. Contact Us" ;
    //$response .= "5. Confirm transaction" \n;

} else if ($text == "1") {
    // Prompt famrmer to enter details
    $response = "CON Farmer Details: \n";
    $response .= "1. Enter full names \n";
    $response .= "2. Enter phone number \n";
    $response .= "3. Product \n";
    $response .= "4. Quantity of produce(in kg) \n";
    $response .= "5. Season of harvest \n";
    $response .= "6. Location";

}else if(isset($level[0]) && $level[0]!=”” && !isset($level[1])) { 
    $text = "Enter your Full Names:";
    $name  = $level[0];
    $response = "CON Name: ".$name;

} else if(isset($level[1]) && $level[1]!=”” && !isset($level[1])) { 
    $text = "Enter your Phone Number:";
    $phone_number  = $level[1];
    $response = "CON Phone Number: ".$phone_number;

}  else if(isset($level[2]) && $level[2]!=”” && !isset($level[1])) { 
    $text = "Enter Product:";
    $product  = $level[2];
    $response = "CON Product: ".$product;

}  else if (isset($level[3]) && $level[3]!=”” && !isset($level[1])) { 
    $text = "Enter Quantity:";
    $quantity  = $level[3];
    $response = "CON Quantity: ".$quantity;

}  else if(isset($level[4]) && $level[4]!=”” && !isset($level[1])) { 
    $text = "Enter Season:";
    $season  = $level[4];
    $response = "CON Season: ".$season;

}else if(isset($level[5]) && $level[5]!=”” && !isset($level[1])) { 
    $text = "Enter Location:";
    $location  = $level[5];
    $response = "END Location: ".$location;

} else if ($text == "2") {
    // Prompt buyer to enter details
    $response = "CON Buyer Details: \n";
    $response .= "1. Enter full names \n";
    $response .= "2. Enter phone number \n";
    $response .= "3. Location";

} else if(isset($level[0]) && $level[0]!=”” && !isset($level[2])) { 
    $text = "Enter your Full Names:";
    $bname  = $level[0];
    $response .= "CON Name: ".$bname;

} else if(isset($level[1]) && $level[1]!=”” && !isset($level[2])) { 
    $text = "Enter your Phone Number:";
    $bphone_number  = $level[1];
    $response .= "CON Phone Number: ".$bphone_number;

}  else if(isset($level[2]) && $level[2]!=”” && !isset($level[2])) { 
    $text = "Enter Location:";
    $blocation  = $level[2];
    $response .= "END Location: ".$blocation;

} else  if ($text == "3") {
    // Description on Mtazao
    $response = "END Mtazao is a mobile platform that connects farmers and buyers directly.For more information log on to our website. \n";

    //else if to else

} else  if ($text == "4") {
    // Description on Mtazao
    $response = "END In case of any queries and concerns contact us on:0712-345-678 or visit our website for more information. Or send an SMS to 26641.";

    //else if to else
}
//else if($text == "4") { 
// Driver's part to confirm delivery
//$response = "Driver details ".$;


// Echo the response back to the API
header('Content-type: text/plain');
echo $response;